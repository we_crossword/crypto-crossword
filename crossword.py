def fill_yel_till_B_row(grid, i, j):
    for k in range(i, len(grid)):
        if grid[k][j] == 'B':
            break
        else:
            grid[k][j] = 'Y'

def fill_yel_till_B_col(grid, i, j):
    for k in range(j, len(grid[0])):
        if grid[i][k] == 'B':
            break
        else:
            grid[i][k] = 'Y'

def iswhite_tillblack_row(grid, row, col, length):
    for _ in range(col + 1, length):
        if grid[row][_] == "W":
            return True
        elif grid[row][_] == 'B':
            break
    return False

def iswhite_tillblack_col(grid, row, col, length):
    for _ in range(row + 1, length):
        if grid[_][col] == "W":
            return True
        elif grid[_][col] == 'B':
            break
    return False

def crossword(grid):
    length = len(grid[0])
    grid_with_nums = []
    num = 1

    def dfs_fill(row, col):
        stack = [(row, col)]
        while stack:
            r, c = stack.pop()
            if grid[r][c] == 'W':
                grid[r][c] = 'Y'
                grid_with_nums.append((r, c, num))
                if r + 1 < length and grid[r + 1][c] == 'W':
                    stack.append((r + 1, c))
                if c + 1 < length and grid[r][c + 1] == 'W':
                    stack.append((r, c + 1))

    for i in range(length):
        for j in range(length):
            if grid[i][j] == "W":
                if (i == 0 or grid[i - 1][j] == 'B') and (j == 0 or grid[i][j - 1] == 'B'):
                    dfs_fill(i, j)
                    num += 1

    return grid_with_nums
